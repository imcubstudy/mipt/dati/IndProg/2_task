#include <set>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <iomanip>

enum class ProblemConstants: uint32_t
{
    char_amount     = 62,
    sum_modifier    = 0x99,
    hash_magic      = 0xedb88320
};

const std::string PermitedSymbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

int ctoi(char c)
{
    if(c <= '9') {
        return c - '0';
    } else if(c <= 'Z') {
        return (int)('9' - '0' + 1) + c - 'A';
    } else if(c <= 'z') {
        return (int)('9' - '0' + 1) + (int)('Z' - 'A' + 1) + c - 'a';
    }
    return -1;
}

class CharSet
{
public:
    std::vector<uint8_t>    value;
    uint8_t                 sum;

    CharSet():
        value(static_cast<int>(ProblemConstants::char_amount), 0),
        sum(0)
    {}

    CharSet(const CharSet& that):
        value(that.value),
        sum(that.sum)
    {}

    bool operator<(const CharSet& that) const
    {
        for(int i = 0; i < static_cast<int>(ProblemConstants::char_amount); ++i)
        {
            if(value[i] < that.value[i]) {
                return true;
            }
        }
        return false;
    }

    CharSet operator+(char c) const
    {
        CharSet result = *this;
        result.value[ctoi(c)]++;
        result.sum += c ^ static_cast<int>(ProblemConstants::sum_modifier);
        return result;
    }
};

int gettarget(std::string login)
{
    int result = 0xffffffff;

    for (char c: login)
    {
        result = result ^ c;

        for (int i = 7; i >= 0; i--)
        {
            int temp = (-(result & 0x1)) & static_cast<uint32_t>(ProblemConstants::hash_magic);
            result = (((unsigned)result) >> 1) ^ temp;
        }
    }

    return ~result & 0xff;
}

void pushpasswords(std::vector<std::string>& passwords, const CharSet& set)
{
    std::string password;
    for(auto c: PermitedSymbols) {
        for(int i = 0; i < set.value[ctoi(c)]; ++i) {
            password += c;
        }
    }
    std::sort(std::begin(password), std::end(password));

    do {
        passwords.push_back(password);
    } while(std::next_permutation(std::begin(password), std::end(password)));
}

int main(int argc, char *argv[])
{
    if(argc != 3) {
        return -1;
    }

    int target = gettarget(argv[1]);
    int LEN = std::stoi(argv[2]);

    std::vector<std::set<CharSet>> *prev = new std::vector<std::set<CharSet>>(0xff + 1);
    std::vector<std::set<CharSet>> *next = new std::vector<std::set<CharSet>>(0xff + 1);
    for(char c: PermitedSymbols) {
        (*prev)[c ^ static_cast<uint8_t>(ProblemConstants::sum_modifier)].insert(CharSet() + c);
    }

    for(int len = 2; len <= LEN; len++) {
        for(int sum = 0x00; sum <= 0xff; ++sum) {
            std::cout << "Generating passwords of len " << len << " "
                      << std::fixed << std::setw(3) << std::setfill('0') << std::setprecision(0) 
                      << ((float)sum / 0xff * 100) << "%" << std::flush;
            for(auto& cset: (*prev)[sum]) {
                for(char c: PermitedSymbols) {
                    CharSet tmp = cset + c;
                    (*next)[tmp.sum].insert(tmp);
                }
            }
            if(sum != 0xff) {
                std::cout << "\r";
            } else {
                std::cout << std::endl;
            }
        }

        for(auto& s: *prev) {
            s.clear();
        }
        std::swap(prev, next);
    }
    std::cout << std::endl;

    std::vector<std::string> passwords;
    for(auto &cs: (*prev)[target]) {
        pushpasswords(passwords, cs);
    }

    for(auto pass: passwords) {
        std::cout << pass << std::endl;
    }

    delete prev;
    delete next;

    return 0;
}
