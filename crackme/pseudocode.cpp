bool Validate(string input)
{
	for (char in input)
	{
		if (char not in [0-9, A-Z, a-z])
		{
			return false;
		}
	}
	
	return true;
}

int ProcessLogin(string login)
{
	int result = 0xff'ff'ff'ff;
	int temp = 0;
	
	for (char in login)
	{
		result = result XOR char
		
		for (int i = 7; i >= 0; i--)
		{
			temp = result
			
			//--some asm:--
			//and temp, 1
			//neg temp
			//shr result, 1
			//and temp, 0xedb88320
			//--asm end--
			
			result = result XOR temp
		}
	}
	
	return result;
}

int ProcessPass(string pass)
{
	int result = 0;
	for (char in pass)
	{
		result = result + (char XOR 0x99)
	}
	
	return result;
}

bool ProcessCred(string login, string password)
{
	int loginResult = ProcessLogin(login);
	int passResult = ProcessPass(login);
	
	//--some asm:--
	//not loginResult
	//and loginResult, 0xff
	//and passResult, 0xff
	//--asm end--
	
	if (loginResult == passResult)
	{
		return true;
	}
	
	return false;
}

main() {
	string login
	string password
	bool result

	cin >> login;
	if (!Validate(login)) return ERROR;
	
	
	cin >> password;
	if (!Validate(password)) return ERROR; 

	if (!ProcessCred(login, password)) return ERROR;
	
	return SUCCESS;
}
